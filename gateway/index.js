const express = require('express')
const http = require('http')
const { Server } = require('socket.io')
const api = require('./../api')

const app = express()
const server = http.createServer(app)
const io = new Server(server)

server.listen(80, async () => {
  console.log('server initialize')

  io.on('connection', socket => {
    console.log('New connection', socket.id)

    socket.on('req:microservice:create', async ({ name, descripcion, url, video, imagen }) => {
      try {
        console.log('req:microservice:create', ({ name, descripcion, url, video, imagen }))

        const { statusCode, data, message } = await api.Create({ name, descripcion, url, video, imagen })
        return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message })
      } catch (error) {
        console.log(error)
      }
    })

    socket.on('req:microservice:delete', async ({ id }) => {
      try {
        console.log('req:microservice:delete', ({ id }))

        const { statusCode, data, message } = await api.Delete({ id })
        return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message })
      } catch (error) {
        console.log(error)
      }
    })

    socket.on('req:microservice:update', async ({ name, descripcion, url, video, imagen, id }) => {
      console.log('req:microservice:update', ({ name, descripcion, url, video, imagen, id }))
      try {
        console.log('req:microservice:update', ({ name, descripcion, url, video, imagen, id }))

        const { statusCode, data, message } = await api.Update({ name, descripcion, url, video, imagen, id })
        return io.to(socket.id).emit('res:microservice:update', { statusCode, data, message, id })
      } catch (error) {
        console.log(error)
      }
    })

    socket.on('req:microservice:findOne', async ({ id }) => {
      try {
        console.log('req:microservice:findOne', ({ id }))

        const { statusCode, data, message } = await api.FindOne({ id })
        return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message })
      } catch (error) {
        console.log(error)
      }
    })

    socket.on('req:microservice:view', async ({}) => {
      try {
        console.log('req:microservice:view', ({}))

        const { statusCode, data, message } = await api.View({})
        return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message })
      } catch (error) {
        console.log(error)
      }
    })
  })
})