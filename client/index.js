const { io } = require('socket.io-client')

const socket = io('http://192.168.100.25')
const main = async () => {
  try {
    setTimeout(() => console.log(socket.id), 1000)

    socket.on('res:microservice:create', ({ statusCode, data, message }) => console.log('res:microservice:create', { statusCode, data, message }))
    socket.on('res:microservice:delete', ({ statusCode, data, message }) => console.log('res:microservice:delete', { statusCode, data, message }))
    socket.on('res:microservice:update', ({ statusCode, data, message }) => console.log('res:microservice:update', { statusCode, data, message }))
    socket.on('res:microservice:findOne', ({ statusCode, data, message }) => console.log('res:microservice:findOne', { statusCode, data, message }))
    socket.on('res:microservice:view', ({ statusCode, data, message }) => console.log('res:microservice:view', { statusCode, data, message }))
    
    const data = {
      name: 'Nuevo', 
      descripcion: 'Campaign', 
      url: 'www.google.com', 
      video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
      imagen: 'image.jpg',
      // id: 5
    }

    // setInterval(() => socket.emit('req:microservice:view', ({})), 1000)
    socket.emit('req:microservice:create', (data))
    // socket.emit('req:microservice:update', (data))
    // socket.emit('req:microservice:findOne', ({ id: 5 }))

    // socket.emit('req:microservice:delete', ({ id: 6 }))
    // socket.emit('req:microservice:delete', ({ id: 5 }))
    // socket.emit('req:microservice:delete', ({ id: 4 }))

    socket.emit('req:microservice:view', ({}))
  } catch (error) {
    console.log(error)
  }
}

main()