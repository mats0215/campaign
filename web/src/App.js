import React, { useEffect, useState } from 'react';
const { io } = require('socket.io-client')

function App() {
  const [id, setId] = useState()
  const [data, setData] = useState([])

  useEffect(() => {
    const socket = io('http://192.168.100.25', { transports: ['websocket'], jsonp: false })
    setTimeout(() => setId(socket.id), 1000)

    socket.on('res:microservice:create', ({ statusCode, data, message }) => console.log('res:microservice:create', { statusCode, data, message }))
    socket.on('res:microservice:delete', ({ statusCode, data, message }) => console.log('res:microservice:delete', { statusCode, data, message }))
    socket.on('res:microservice:update', ({ statusCode, data, message }) => console.log('res:microservice:update', { statusCode, data, message }))
    socket.on('res:microservice:findOne', ({ statusCode, data, message }) => console.log('res:microservice:findOne', { statusCode, data, message }))
    socket.on('res:microservice:view', ({ statusCode, data, message }) => {
      console.log('res:microservice:view', { statusCode, data, message })
      if(statusCode === 200) setData(data)
    })

    socket.emit('req:microservice:view', ({}))

  }, [])

  return (
    <div>
      <p>{ id ? 'Online: ' + id : 'Offline' }</p>

      <table className='table-auto table-fixed'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Desc.</th>
            <th>Url</th>
            <th>Video</th>
            <th>Imagen</th>
          </tr>
        </thead>
        <tbody>
          {
            data.map(v => (
              <tr>
                <td>{ v.name }</td>
                <td>{ v.descripcion }</td>
                <td>{ v.url }</td>
                <td>{ v.video }</td>
                <td>{ v.imagen }</td>
                <td>{ v.worker }</td>
              </tr>
            
            ))
          }

        </tbody>
      </table>
    </div>
  );
}

export default App;
