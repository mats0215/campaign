const bull = require('bull')

const options = { redis: { host: '192.168.100.25', port: 6379 } }

const queueCreate = bull('campaign:create', options)
const queueDelete = bull('campaign:delete', options)
const queueUpdate = bull('campaign:update', options)
const queueFindOne = bull('campaign:findOne', options)
const queueView = bull('campaign:view', options)

const Create = async ({ name, descripcion, url, video, imagen }) => {
  try {
    const job = await queueCreate.add({ name, descripcion, url, video, imagen })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Delete = async ({ id }) => {
  try {
    const job = await queueDelete.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Update = async ({ name, descripcion, url, video, imagen, id }) => {
  try {
    const job = await queueUpdate.add({ name, descripcion, url, video, imagen, id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const FindOne = async ({ id }) => {
  try {
    const job = await queueFindOne.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const View = async ({}) => {
  try {
    const job = await queueView.add({})
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const main = async () => {
  // await Create({
  //   name: 'Mar', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg'
  // })

  // await Update({
  //   name: 'Luis MOD', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg',
  //   id: 1
  // })

  // await FindOne({ id: 1 })

  // await Delete({ id: 3 })

  // await View({})
}

module.exports = { Create, Delete, Update, FindOne, View }