const { Model } = require('./../Models')

const Create = async ({ name, description, url, video, image }) => {
  try {
    const instance = await Model.create(
      { name, description, url, video, image }, 
      { fields: ['name', 'description', 'url', 'video', 'image'] }
    )

    return { statusCode: 200, data: instance.toJSON() }
  } catch (error) {
    console.log({ step: 'controller Create', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Delete = async ({ where = {} }) => {
  try {
    await Model.destroy({ where })

    return { statusCode: 200, data: 'ok' }
  } catch (error) {
    console.log({ step: 'controller Delete', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Update = async ({ name, description, url, video, image, id }) => {
  try {
    const instance = await Model.update(
      { name, description, url, video, image, id }, 
      { where: { id } }
    )

    return { statusCode: 200, data: instance[1][0].toJSON() }
  } catch (error) {
    console.log({ step: 'controller Update', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const View = async ({ where = {} }) => {
  try {
    const instances = await Model.findAll({ where })
    
    return { statusCode: 200, data: instances }
  } catch (error) {
    console.log({ step: 'controller View', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

module.exports = { Create, Update, Delete, View }