const { sequelize } = require('./../settings')
const { DataTypes } = require('sequelize')

const Model = sequelize.define('campaign', {
	name: { type: DataTypes.STRING },
	descripcion: { type: DataTypes.STRING },
	url: { type: DataTypes.STRING },
	video: { type: DataTypes.STRING },
	imagen: { type: DataTypes.STRING }
})

const SyncDB = async () => {
	try {
		console.log('initializing database')

		await Model.sync({ logging: false })
		console.log('initialized database')

		return { statusCode: 200, data: 'ok' }
	} catch (error) {
		console.log(error)
		return { statusCode: 500, message: error.toString() }
	}
}

module.exports = { SyncDB, Model }