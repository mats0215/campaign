const bull = require('bull')
const { redis } = require('./../settings')

const options = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull('campaign:create', options)
const queueDelete = bull('campaign:delete', options)
const queueUpdate = bull('campaign:update', options)
const queueFindOne = bull('campaign:findOne', options)
const queueView = bull('campaign:view', options)

module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView }